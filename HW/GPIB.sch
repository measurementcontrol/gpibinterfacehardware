EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "AVR GPIB Interface"
Date "2019-06-25"
Rev "v1.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GPIB-packed:ATMEGA328-AU++atmel U2
U 1 1 5C95B969
P 5850 2525
F 0 "U2" H 5900 3892 50  0000 C CNN
F 1 "ATMEGA328-AU" H 5900 3801 50  0000 C CNN
F 2 "Housings_QFP:TQFP-32_7x7mm_Pitch0.8mm" H 5850 2525 50  0001 C CIN
F 3 "http://www.atmel.com/images/atmel-8271-8-bit-avr-microcontroller-atmega48a-48pa-88a-88pa-168a-168pa-328-328p_datasheet.pdf" H 5850 2525 50  0001 C CNN
	1    5850 2525
	1    0    0    -1  
$EndComp
$Comp
L GPIB-packed:CH340G++suf_ic_usb U1
U 1 1 5C95BA83
P 2675 1800
F 0 "U1" H 2675 2397 60  0000 C CNN
F 1 "CH340G" H 2675 2291 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 2675 1800 60  0001 C CNN
F 3 "" H 2675 1800 60  0000 C CNN
	1    2675 1800
	1    0    0    -1  
$EndComp
$Comp
L GPIB-packed:USB_B++conn J1
U 1 1 5C95BB36
P 1100 1850
F 0 "J1" H 1155 2317 50  0000 C CNN
F 1 "USB" H 1155 2226 50  0000 C CNN
F 2 "Connect:USB_B" H 1250 1800 50  0001 C CNN
F 3 "" H 1250 1800 50  0001 C CNN
	1    1100 1850
	1    0    0    -1  
$EndComp
$Comp
L GPIB-packed:Conn_02x12_Top_Bottom++conn J2
U 1 1 5C95BC35
P 9525 2450
F 0 "J2" H 9575 3167 50  0000 C CNN
F 1 "IEEE 488" H 9575 3076 50  0000 C CNN
F 2 "suf_connector_misc:TE_CHAMP_24_DEG90" H 9525 2450 50  0001 C CNN
F 3 "~" H 9525 2450 50  0001 C CNN
	1    9525 2450
	1    0    0    -1  
$EndComp
Text GLabel 9225 1950 0    50   Input ~ 0
DIO1
Text GLabel 9225 2050 0    50   Input ~ 0
DIO2
Text GLabel 9225 2150 0    50   Input ~ 0
DIO3
Text GLabel 9225 2250 0    50   Input ~ 0
DIO4
Text GLabel 9225 2350 0    50   Input ~ 0
EOI
Text GLabel 9225 2450 0    50   Input ~ 0
DAV
Text GLabel 9225 2550 0    50   Input ~ 0
NRFD
Text GLabel 9225 2650 0    50   Input ~ 0
NDAC
Text GLabel 9225 2750 0    50   Input ~ 0
IFC
Text GLabel 9225 2850 0    50   Input ~ 0
SRQ
Text GLabel 9225 2950 0    50   Input ~ 0
ATN
Text GLabel 9925 1950 2    50   Input ~ 0
DIO5
Text GLabel 9925 2050 2    50   Input ~ 0
DIO6
Text GLabel 9925 2150 2    50   Input ~ 0
DIO7
Text GLabel 9925 2250 2    50   Input ~ 0
DIO8
Text GLabel 9925 2350 2    50   Input ~ 0
REN
$Comp
L GPIB-packed:GND++power #PWR018
U 1 1 5C95BFAF
P 9575 3200
F 0 "#PWR018" H 9575 2950 50  0001 C CNN
F 1 "GND" H 9580 3027 50  0000 C CNN
F 2 "" H 9575 3200 50  0001 C CNN
F 3 "" H 9575 3200 50  0001 C CNN
	1    9575 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9325 3050 9275 3050
Wire Wire Line
	9275 3050 9275 3200
Wire Wire Line
	9275 3200 9575 3200
Wire Wire Line
	9575 3200 9900 3200
Wire Wire Line
	9900 3200 9900 3050
Wire Wire Line
	9900 2450 9825 2450
Connection ~ 9575 3200
Wire Wire Line
	9825 2550 9900 2550
Connection ~ 9900 2550
Wire Wire Line
	9900 2550 9900 2450
Wire Wire Line
	9825 2650 9900 2650
Connection ~ 9900 2650
Wire Wire Line
	9900 2650 9900 2550
Wire Wire Line
	9825 2750 9900 2750
Connection ~ 9900 2750
Wire Wire Line
	9900 2750 9900 2650
Wire Wire Line
	9825 2850 9900 2850
Connection ~ 9900 2850
Wire Wire Line
	9900 2850 9900 2750
Wire Wire Line
	9825 2950 9900 2950
Connection ~ 9900 2950
Wire Wire Line
	9900 2950 9900 2850
Wire Wire Line
	9825 3050 9900 3050
Connection ~ 9900 3050
Wire Wire Line
	9900 3050 9900 2950
Wire Wire Line
	9325 1950 9225 1950
Wire Wire Line
	9225 2050 9325 2050
Wire Wire Line
	9225 2150 9325 2150
Wire Wire Line
	9225 2250 9325 2250
Wire Wire Line
	9225 2350 9325 2350
Wire Wire Line
	9225 2450 9325 2450
Wire Wire Line
	9225 2550 9325 2550
Wire Wire Line
	9225 2650 9325 2650
Wire Wire Line
	9225 2750 9325 2750
Wire Wire Line
	9225 2850 9325 2850
Wire Wire Line
	9225 2950 9325 2950
Wire Wire Line
	9825 1950 9925 1950
Wire Wire Line
	9825 2050 9925 2050
Wire Wire Line
	9825 2150 9925 2150
Wire Wire Line
	9825 2250 9925 2250
Wire Wire Line
	9825 2350 9925 2350
$Comp
L GPIB-packed:AVR-ISP-6++atmel J3
U 1 1 5C95FA25
P 2275 3725
F 0 "J3" H 2262 4090 50  0000 C CNN
F 1 "ISP" H 2262 3999 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm" V 1755 3765 50  0001 C CNN
F 3 "" H 2250 3725 50  0001 C CNN
	1    2275 3725
	1    0    0    -1  
$EndComp
Text GLabel 6950 2275 2    50   Input ~ 0
DIO1
Text GLabel 6950 2375 2    50   Input ~ 0
DIO2
Text GLabel 6950 2475 2    50   Input ~ 0
DIO3
Text GLabel 6950 2575 2    50   Input ~ 0
DIO4
Text GLabel 6950 2675 2    50   Input ~ 0
DIO5
Text GLabel 6950 2775 2    50   Input ~ 0
DIO6
Text GLabel 6950 3425 2    50   Input ~ 0
DIO7
Text GLabel 6950 3525 2    50   Input ~ 0
DIO8
Text GLabel 6950 1825 2    50   Input ~ 0
EOI
Text GLabel 6950 1725 2    50   Input ~ 0
DAV
Text GLabel 6950 1625 2    50   Input ~ 0
NRFD
Text GLabel 6950 1525 2    50   Input ~ 0
NDAC
Text GLabel 6950 1425 2    50   Input ~ 0
IFC
Text GLabel 6950 3725 2    50   Input ~ 0
ATN
Wire Wire Line
	6950 1425 6850 1425
Wire Wire Line
	6950 1525 6850 1525
Wire Wire Line
	6850 1625 6950 1625
Wire Wire Line
	6850 1725 6900 1725
Wire Wire Line
	6950 1825 6900 1825
Wire Wire Line
	6950 2275 6850 2275
Wire Wire Line
	6850 2375 6950 2375
Wire Wire Line
	6850 2475 6950 2475
Wire Wire Line
	6850 2575 6950 2575
Wire Wire Line
	6850 2675 6950 2675
Wire Wire Line
	6850 2775 6950 2775
Wire Wire Line
	6950 3425 6850 3425
Wire Wire Line
	6950 3525 6850 3525
Wire Wire Line
	6950 3725 6850 3725
$Comp
L GPIB-packed:VCC++power #PWR012
U 1 1 5C967886
P 4900 1300
F 0 "#PWR012" H 4900 1150 50  0001 C CNN
F 1 "VCC" H 4917 1473 50  0000 C CNN
F 2 "" H 4900 1300 50  0001 C CNN
F 3 "" H 4900 1300 50  0001 C CNN
	1    4900 1300
	1    0    0    -1  
$EndComp
$Comp
L GPIB-packed:GND++power #PWR014
U 1 1 5C9678C7
P 4900 3825
F 0 "#PWR014" H 4900 3575 50  0001 C CNN
F 1 "GND" H 4905 3652 50  0000 C CNN
F 2 "" H 4900 3825 50  0001 C CNN
F 3 "" H 4900 3825 50  0001 C CNN
	1    4900 3825
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 3525 4900 3525
Wire Wire Line
	4900 3525 4900 3625
Wire Wire Line
	4950 3625 4900 3625
Connection ~ 4900 3625
Wire Wire Line
	4900 3625 4900 3725
Wire Wire Line
	4950 3725 4900 3725
Connection ~ 4900 3725
Wire Wire Line
	4900 3725 4900 3825
Wire Wire Line
	4900 1300 4900 1425
Wire Wire Line
	4900 1525 4950 1525
Wire Wire Line
	4950 1425 4900 1425
Connection ~ 4900 1425
Wire Wire Line
	4900 1425 4900 1525
Text GLabel 6950 3125 2    50   Input ~ 0
TXD
Text GLabel 6950 3025 2    50   Input ~ 0
RXD
Wire Wire Line
	6850 3025 6950 3025
Wire Wire Line
	6850 3125 6950 3125
Text GLabel 1750 1550 0    50   Input ~ 0
RXD
Text GLabel 1750 1650 0    50   Input ~ 0
TXD
$Comp
L GPIB-packed:R++Device R1
U 1 1 5C96E006
P 2000 1550
F 0 "R1" V 1900 1475 50  0000 C CNN
F 1 "1K" V 1900 1625 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1930 1550 50  0001 C CNN
F 3 "~" H 2000 1550 50  0001 C CNN
	1    2000 1550
	0    1    1    0   
$EndComp
$Comp
L GPIB-packed:R++Device R2
U 1 1 5C96E0A3
P 2000 1650
F 0 "R2" V 2100 1575 50  0000 C CNN
F 1 "1K" V 2100 1725 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1930 1650 50  0001 C CNN
F 3 "~" H 2000 1650 50  0001 C CNN
	1    2000 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	4900 1525 4900 1725
Wire Wire Line
	4900 1725 4950 1725
Connection ~ 4900 1525
Text GLabel 7200 1875 2    50   Input ~ 0
MISO
Text GLabel 6950 1925 2    50   Input ~ 0
SCK
Text GLabel 7200 1775 2    50   Input ~ 0
MOSI
Text GLabel 7400 2875 2    50   Input ~ 0
RST
Wire Wire Line
	7400 2875 7275 2875
Wire Wire Line
	7200 1875 6900 1875
Wire Wire Line
	6900 1875 6900 1825
Connection ~ 6900 1825
Wire Wire Line
	6900 1825 6850 1825
Wire Wire Line
	7200 1775 6900 1775
Wire Wire Line
	6900 1775 6900 1725
Connection ~ 6900 1725
Wire Wire Line
	6900 1725 6950 1725
Wire Wire Line
	6950 1925 6850 1925
Wire Wire Line
	1750 1550 1850 1550
Wire Wire Line
	1750 1650 1850 1650
Wire Wire Line
	2150 1650 2225 1650
$Comp
L GPIB-packed:D_Schottky++Device D1
U 1 1 5C97DEBF
P 1450 1450
F 0 "D1" V 1404 1529 50  0000 L CNN
F 1 "MBR0520LT1G" V 1250 900 50  0000 L CNN
F 2 "Diodes_SMD:D_SOD-123" H 1450 1450 50  0001 C CNN
F 3 "~" H 1450 1450 50  0001 C CNN
	1    1450 1450
	0    1    1    0   
$EndComp
$Comp
L GPIB-packed:VCC++power #PWR02
U 1 1 5C97F9DD
P 1450 1250
F 0 "#PWR02" H 1450 1100 50  0001 C CNN
F 1 "VCC" H 1467 1423 50  0000 C CNN
F 2 "" H 1450 1250 50  0001 C CNN
F 3 "" H 1450 1250 50  0001 C CNN
	1    1450 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 1650 1450 1650
Wire Wire Line
	1450 1650 1450 1600
Wire Wire Line
	1450 1250 1450 1275
$Comp
L GPIB-packed:GND++power #PWR01
U 1 1 5C983285
P 1050 2300
F 0 "#PWR01" H 1050 2050 50  0001 C CNN
F 1 "GND" H 1055 2127 50  0000 C CNN
F 2 "" H 1050 2300 50  0001 C CNN
F 3 "" H 1050 2300 50  0001 C CNN
	1    1050 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 2250 1000 2300
Wire Wire Line
	1000 2300 1050 2300
Wire Wire Line
	1050 2300 1100 2300
Wire Wire Line
	1100 2300 1100 2250
Connection ~ 1050 2300
Wire Wire Line
	1400 1850 1500 1850
Wire Wire Line
	1400 1950 1550 1950
$Comp
L GPIB-packed:Crystal_Small++Device Y1
U 1 1 5C98D5EF
P 2100 2350
F 0 "Y1" H 2100 2575 50  0000 C CNN
F 1 "12MHz" H 2100 2484 50  0000 C CNN
F 2 "Crystals:Crystal_SMD_HC49-SD" H 2100 2350 50  0001 R CNN
F 3 "~" H 2100 2350 50  0001 C CNN
	1    2100 2350
	1    0    0    -1  
$EndComp
$Comp
L GPIB-packed:C_Small++Device C1
U 1 1 5C98D775
P 1975 2500
F 0 "C1" H 1825 2575 50  0000 L CNN
F 1 "22pF" H 1775 2425 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1975 2500 50  0001 C CNN
F 3 "~" H 1975 2500 50  0001 C CNN
	1    1975 2500
	1    0    0    -1  
$EndComp
$Comp
L GPIB-packed:C_Small++Device C3
U 1 1 5C98D8BC
P 2225 2500
F 0 "C3" H 2250 2575 50  0000 L CNN
F 1 "22pF" H 2250 2425 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2225 2500 50  0001 C CNN
F 3 "~" H 2225 2500 50  0001 C CNN
	1    2225 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2225 2050 1975 2050
Wire Wire Line
	1975 2050 1975 2350
Wire Wire Line
	2225 2150 2225 2350
Wire Wire Line
	2000 2350 1975 2350
Connection ~ 1975 2350
Wire Wire Line
	1975 2350 1975 2400
Wire Wire Line
	2200 2350 2225 2350
Connection ~ 2225 2350
Wire Wire Line
	2225 2350 2225 2400
$Comp
L GPIB-packed:GND++power #PWR03
U 1 1 5C996A03
P 1975 2650
F 0 "#PWR03" H 1975 2400 50  0001 C CNN
F 1 "GND" H 1980 2477 50  0000 C CNN
F 2 "" H 1975 2650 50  0001 C CNN
F 3 "" H 1975 2650 50  0001 C CNN
	1    1975 2650
	1    0    0    -1  
$EndComp
$Comp
L GPIB-packed:GND++power #PWR06
U 1 1 5C996A5A
P 2225 2650
F 0 "#PWR06" H 2225 2400 50  0001 C CNN
F 1 "GND" H 2230 2477 50  0000 C CNN
F 2 "" H 2225 2650 50  0001 C CNN
F 3 "" H 2225 2650 50  0001 C CNN
	1    2225 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1975 2600 1975 2650
Wire Wire Line
	2225 2600 2225 2650
$Comp
L GPIB-packed:Crystal_Small++Device Y2
U 1 1 5C99B95C
P 7800 2325
F 0 "Y2" H 8000 2375 50  0000 C CNN
F 1 "16MHz" H 7500 2350 50  0000 C CNN
F 2 "Crystals:Crystal_SMD_HC49-SD" H 7800 2325 50  0001 R CNN
F 3 "~" H 7800 2325 50  0001 C CNN
	1    7800 2325
	-1   0    0    -1  
$EndComp
$Comp
L GPIB-packed:C_Small++Device C9
U 1 1 5C99B962
P 7950 2475
F 0 "C9" H 7800 2550 50  0000 L CNN
F 1 "22pF" H 7750 2400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 7950 2475 50  0001 C CNN
F 3 "~" H 7950 2475 50  0001 C CNN
	1    7950 2475
	-1   0    0    -1  
$EndComp
$Comp
L GPIB-packed:C_Small++Device C8
U 1 1 5C99B968
P 7650 2475
F 0 "C8" H 7675 2550 50  0000 L CNN
F 1 "22pF" H 7450 2400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 7650 2475 50  0001 C CNN
F 3 "~" H 7650 2475 50  0001 C CNN
	1    7650 2475
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7950 2025 7950 2125
Wire Wire Line
	7650 2125 7650 2325
Wire Wire Line
	7900 2325 7950 2325
Connection ~ 7950 2325
Wire Wire Line
	7950 2325 7950 2375
Connection ~ 7650 2325
Wire Wire Line
	7650 2325 7650 2375
$Comp
L GPIB-packed:GND++power #PWR017
U 1 1 5C99B976
P 7950 2625
F 0 "#PWR017" H 7950 2375 50  0001 C CNN
F 1 "GND" H 7955 2452 50  0000 C CNN
F 2 "" H 7950 2625 50  0001 C CNN
F 3 "" H 7950 2625 50  0001 C CNN
	1    7950 2625
	-1   0    0    -1  
$EndComp
$Comp
L GPIB-packed:GND++power #PWR016
U 1 1 5C99B97C
P 7650 2625
F 0 "#PWR016" H 7650 2375 50  0001 C CNN
F 1 "GND" H 7655 2452 50  0000 C CNN
F 2 "" H 7650 2625 50  0001 C CNN
F 3 "" H 7650 2625 50  0001 C CNN
	1    7650 2625
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7950 2575 7950 2625
Wire Wire Line
	7650 2575 7650 2625
Wire Wire Line
	6850 2125 7650 2125
$Comp
L GPIB-packed:VCC++power #PWR09
U 1 1 5C9A98B7
P 3225 1350
F 0 "#PWR09" H 3225 1200 50  0001 C CNN
F 1 "VCC" H 3242 1523 50  0000 C CNN
F 2 "" H 3225 1350 50  0001 C CNN
F 3 "" H 3225 1350 50  0001 C CNN
	1    3225 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3125 1450 3225 1450
Wire Wire Line
	3225 1450 3225 1350
$Comp
L GPIB-packed:C_Small++Device C4
U 1 1 5C9AC78C
P 3300 1750
F 0 "C4" H 3325 1825 50  0000 L CNN
F 1 "100nF" H 3325 1675 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3300 1750 50  0001 C CNN
F 3 "~" H 3300 1750 50  0001 C CNN
	1    3300 1750
	0    1    1    0   
$EndComp
Text GLabel 3475 1750 2    50   Input ~ 0
RST
Wire Wire Line
	3125 1750 3200 1750
Wire Wire Line
	3400 1750 3475 1750
$Comp
L GPIB-packed:R++Device R3
U 1 1 5C9BAB84
P 7275 2650
F 0 "R3" H 7345 2696 50  0000 L CNN
F 1 "10K" H 7345 2605 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 7205 2650 50  0001 C CNN
F 3 "~" H 7275 2650 50  0001 C CNN
	1    7275 2650
	1    0    0    -1  
$EndComp
$Comp
L GPIB-packed:VCC++power #PWR015
U 1 1 5C9BACEF
P 7275 2425
F 0 "#PWR015" H 7275 2275 50  0001 C CNN
F 1 "VCC" H 7292 2598 50  0000 C CNN
F 2 "" H 7275 2425 50  0001 C CNN
F 3 "" H 7275 2425 50  0001 C CNN
	1    7275 2425
	1    0    0    -1  
$EndComp
Wire Wire Line
	7275 2425 7275 2500
Wire Wire Line
	7275 2800 7275 2875
Connection ~ 7275 2875
Wire Wire Line
	7275 2875 6850 2875
$Comp
L GPIB-packed:VCC++power #PWR07
U 1 1 5C9C237B
P 2650 3225
F 0 "#PWR07" H 2650 3075 50  0001 C CNN
F 1 "VCC" H 2667 3398 50  0000 C CNN
F 2 "" H 2650 3225 50  0001 C CNN
F 3 "" H 2650 3225 50  0001 C CNN
	1    2650 3225
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 3625 2650 3575
Wire Wire Line
	2650 3225 2650 3275
Wire Wire Line
	2650 3625 2375 3625
$Comp
L GPIB-packed:GND++power #PWR08
U 1 1 5C9C8C3A
P 2650 3925
F 0 "#PWR08" H 2650 3675 50  0001 C CNN
F 1 "GND" H 2655 3752 50  0000 C CNN
F 2 "" H 2650 3925 50  0001 C CNN
F 3 "" H 2650 3925 50  0001 C CNN
	1    2650 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	2375 3825 2650 3825
Wire Wire Line
	2650 3825 2650 3925
Text GLabel 1750 3825 0    50   Input ~ 0
RST
Wire Wire Line
	2125 3825 1750 3825
Text GLabel 1750 3625 0    50   Input ~ 0
MISO
Text GLabel 2725 3725 2    50   Input ~ 0
MOSI
Text GLabel 1750 3725 0    50   Input ~ 0
SCK
Wire Wire Line
	2725 3725 2375 3725
Wire Wire Line
	2125 3725 1750 3725
Wire Wire Line
	2125 3625 1750 3625
NoConn ~ 4950 2775
NoConn ~ 4950 2875
$Comp
L GPIB-packed:C_Small++Device C7
U 1 1 5C9E602F
P 4900 2175
F 0 "C7" H 4925 2250 50  0000 L CNN
F 1 "100nF" H 4925 2100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4900 2175 50  0001 C CNN
F 3 "~" H 4900 2175 50  0001 C CNN
	1    4900 2175
	-1   0    0    -1  
$EndComp
$Comp
L GPIB-packed:GND++power #PWR013
U 1 1 5C9E6035
P 4900 2325
F 0 "#PWR013" H 4900 2075 50  0001 C CNN
F 1 "GND" H 4905 2152 50  0000 C CNN
F 2 "" H 4900 2325 50  0001 C CNN
F 3 "" H 4900 2325 50  0001 C CNN
	1    4900 2325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4900 2275 4900 2325
Wire Wire Line
	4900 2075 4900 2025
Wire Wire Line
	4900 2025 4950 2025
$Comp
L GPIB-packed:GND++power #PWR04
U 1 1 5C9EE1FD
P 2100 1200
F 0 "#PWR04" H 2100 950 50  0001 C CNN
F 1 "GND" H 2105 1027 50  0000 C CNN
F 2 "" H 2100 1200 50  0001 C CNN
F 3 "" H 2100 1200 50  0001 C CNN
	1    2100 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 1200 2100 1175
Wire Wire Line
	2100 1175 2225 1175
Wire Wire Line
	2225 1175 2225 1450
NoConn ~ 3125 1550
NoConn ~ 3125 1650
NoConn ~ 3125 1850
NoConn ~ 3125 1950
NoConn ~ 3125 2050
NoConn ~ 3125 2150
$Comp
L GPIB-packed:C_Small++Device C6
U 1 1 5CA0AAB2
P 4650 1700
F 0 "C6" H 4525 1775 50  0000 L CNN
F 1 "100nF" H 4400 1625 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4650 1700 50  0001 C CNN
F 3 "~" H 4650 1700 50  0001 C CNN
	1    4650 1700
	-1   0    0    -1  
$EndComp
$Comp
L GPIB-packed:GND++power #PWR011
U 1 1 5CA0AAB8
P 4650 1850
F 0 "#PWR011" H 4650 1600 50  0001 C CNN
F 1 "GND" H 4655 1677 50  0000 C CNN
F 2 "" H 4650 1850 50  0001 C CNN
F 3 "" H 4650 1850 50  0001 C CNN
	1    4650 1850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4650 1800 4650 1850
Wire Wire Line
	4900 1525 4650 1525
Wire Wire Line
	4650 1525 4650 1600
$Comp
L GPIB-packed:C_Small++Device C5
U 1 1 5CA13104
P 3775 1600
F 0 "C5" H 3625 1675 50  0000 L CNN
F 1 "100nF" H 3575 1525 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3775 1600 50  0001 C CNN
F 3 "~" H 3775 1600 50  0001 C CNN
	1    3775 1600
	-1   0    0    -1  
$EndComp
$Comp
L GPIB-packed:GND++power #PWR010
U 1 1 5CA1310A
P 3775 1750
F 0 "#PWR010" H 3775 1500 50  0001 C CNN
F 1 "GND" H 3780 1577 50  0000 C CNN
F 2 "" H 3775 1750 50  0001 C CNN
F 3 "" H 3775 1750 50  0001 C CNN
	1    3775 1750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3775 1700 3775 1750
Wire Wire Line
	3225 1450 3775 1450
Wire Wire Line
	3775 1450 3775 1500
Connection ~ 3225 1450
$Comp
L GPIB-packed:R++Device R4
U 1 1 5CA1BF94
P 7800 2125
F 0 "R4" V 7750 2325 50  0000 C CNN
F 1 "1M" V 7875 2125 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 7730 2125 50  0001 C CNN
F 3 "~" H 7800 2125 50  0001 C CNN
	1    7800 2125
	0    1    1    0   
$EndComp
Connection ~ 7650 2125
Connection ~ 7950 2125
Wire Wire Line
	7950 2125 7950 2325
Wire Wire Line
	7650 2325 7700 2325
Wire Wire Line
	6850 2025 7950 2025
$Comp
L GPIB-packed:C_Small++Device C2
U 1 1 5CA2E463
P 2200 1025
F 0 "C2" H 2225 1100 50  0000 L CNN
F 1 "10nF" H 2225 950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2200 1025 50  0001 C CNN
F 3 "~" H 2200 1025 50  0001 C CNN
	1    2200 1025
	-1   0    0    1   
$EndComp
Wire Wire Line
	2150 1550 2225 1550
$Comp
L GPIB-packed:VCC++power #PWR05
U 1 1 5CA3BCDE
P 2200 875
F 0 "#PWR05" H 2200 725 50  0001 C CNN
F 1 "VCC" H 2217 1048 50  0000 C CNN
F 2 "" H 2200 875 50  0001 C CNN
F 3 "" H 2200 875 50  0001 C CNN
	1    2200 875 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 875  2200 925 
Wire Wire Line
	2200 1125 2200 1750
Wire Wire Line
	2200 1750 2225 1750
$Comp
L GPIB-packed:D_Schottky++Device D2
U 1 1 5CA45309
P 2650 3425
F 0 "D2" V 2604 3504 50  0000 L CNN
F 1 "MBR0520LT1G" V 2695 3504 50  0000 L CNN
F 2 "Diodes_SMD:D_SOD-123" H 2650 3425 50  0001 C CNN
F 3 "~" H 2650 3425 50  0001 C CNN
	1    2650 3425
	0    1    1    0   
$EndComp
Text GLabel 6950 3225 2    50   Input ~ 0
SRQ
Text GLabel 6950 3325 2    50   Input ~ 0
REN
Wire Wire Line
	6950 3225 6850 3225
Wire Wire Line
	6950 3325 6850 3325
NoConn ~ 6850 3625
$Comp
L GPIB-packed:PWR_FLAG++power #FLG0101
U 1 1 5CA7EE32
P 1500 1275
F 0 "#FLG0101" H 1500 1350 50  0001 C CNN
F 1 "PWR_FLAG" V 1500 1403 50  0001 L CNN
F 2 "" H 1500 1275 50  0001 C CNN
F 3 "~" H 1500 1275 50  0001 C CNN
	1    1500 1275
	0    1    1    0   
$EndComp
Wire Wire Line
	1500 1275 1450 1275
Connection ~ 1450 1275
Wire Wire Line
	1450 1275 1450 1300
$Comp
L GPIB-packed:USB_B++conn J4
U 1 1 5CA8C848
P 1100 3025
F 0 "J4" H 1155 3492 50  0000 C CNN
F 1 "USB" H 1155 3401 50  0000 C CNN
F 2 "Connect:USB_B" H 1250 2975 50  0001 C CNN
F 3 "" H 1250 2975 50  0001 C CNN
	1    1100 3025
	1    0    0    -1  
$EndComp
$Comp
L GPIB-packed:GND++power #PWR019
U 1 1 5CA8C84E
P 1050 3475
F 0 "#PWR019" H 1050 3225 50  0001 C CNN
F 1 "GND" H 1055 3302 50  0000 C CNN
F 2 "" H 1050 3475 50  0001 C CNN
F 3 "" H 1050 3475 50  0001 C CNN
	1    1050 3475
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 3425 1000 3475
Wire Wire Line
	1000 3475 1050 3475
Wire Wire Line
	1050 3475 1100 3475
Wire Wire Line
	1100 3475 1100 3425
Connection ~ 1050 3475
Wire Wire Line
	1400 2825 1450 2825
Wire Wire Line
	1450 2825 1450 1650
Connection ~ 1450 1650
Wire Wire Line
	1400 3025 1500 3025
Wire Wire Line
	1500 3025 1500 1850
Connection ~ 1500 1850
Wire Wire Line
	1500 1850 2225 1850
Wire Wire Line
	1400 3125 1550 3125
Wire Wire Line
	1550 3125 1550 1950
Connection ~ 1550 1950
Wire Wire Line
	1550 1950 2225 1950
$EndSCHEMATC
